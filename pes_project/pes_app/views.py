from django.shortcuts import render
from django.template import loader, RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from .models import *
from .form import *
from django.core.cache import cache
def indexView(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render())
# Create your views here.
def ciudadView(request):
    template = loader.get_template('ciudad.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = ciudadForm(request.POST)
    #es un selec * from a tipoUsuario para mostrar datos en tabla

    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_ciudad = request.POST.get('nombre','')
        #insertamos y guardamos los datos del formulario en BD
        db_register = ciudad()
        db_register.nombre = nombre_ciudad
        db_register.save()
        return HttpResponseRedirect('/ciudadlist')
    return HttpResponse(template.render(contex, request))

def ciudadlistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = ciudad.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "ciudad_list.html" , contex )

def ciudadUpdateView(request):
    template = loader.get_template('ciudad_update.html')
    id = request.GET['id']
    print(id)
    filtrar = ciudad.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = ciudadForm(initial={'nombre': filtrar[0].nombre})
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = ciudad.objects.all()
    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_ciudad = request.POST.get('nombre', '')
            # insertamos y guardamos los datos del formulario en BD
            db_register = ciudad()
            db_register.id = id
            db_register.nombre = nombre_ciudad
            db_register.save()
            return HttpResponseRedirect('/ciudadlist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def ciudadDeleteView(request):
    template = loader.get_template('ciudad_delete.html')
    id = request.GET['id']

    filtrar = ciudad.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = ciudadForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = ciudad.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/ciudadlist')


######################################################################################################################
def universidadView(request):
    template = loader.get_template('universidad.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = universidadForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_universidad = request.POST.get('nombre','')
        id_ciudad = request.POST.get('ciudad','')
        id_ciudad = ciudad.objects.all().filter(id=id_ciudad)
        #insertamos y guardamos los datos del formulario en BD
        db_register = universidad()
        db_register.nombre = nombre_universidad
        db_register.ciudad = id_ciudad[0]
        db_register.save()
        return HttpResponseRedirect('/universidadlist')
    return HttpResponse(template.render(contex, request))

def universidadlistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = universidad.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "universidad_list.html" , contex )

def universidadUpdateView(request):
    template = loader.get_template('universidad_update.html')
    id = request.GET['id']
    print(id)

    filtrar = universidad.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = universidadForm(initial={'nombre': filtrar[0].nombre, 'ciudad':filtrar[0].ciudad})
    #form = universidadForm()

    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = universidad.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_universidad = request.POST.get('nombre', '')
            id_ciudad = request.POST.get('ciudad','')
            # insertamos y guardamos los datos del formulario en BD
            db_register = universidad()
            db_register.id = id
            db_register.nombre = nombre_universidad
            db_register.ciudad = id_ciudad
            db_register.save()
            return HttpResponseRedirect('/universidadlist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def universidadDeleteView(request):
    template = loader.get_template('universidad_delete.html')
    id = request.GET['id']

    filtrar = universidad.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = universidadForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = universidad.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/universidadlist')

######################################################################################################################
def facultadView(request):
    template = loader.get_template('facultad.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = facultadForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_facultad = request.POST.get('nombre','')
        id_universidad = request.POST.get('universidad','')
        id_universidad = universidad.objects.all().filter(id=id_universidad)
        #insertamos y guardamos los datos del formulario en BD
        db_register = facultad()
        db_register.nombre = nombre_facultad
        db_register.universidad = id_universidad[0]
        db_register.save()
        return HttpResponseRedirect('/facultadlist')
    return HttpResponse(template.render(contex, request))
def facultadlistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = facultad.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "facultad_list.html" , contex )


def facultadUpdateView(request):
    template = loader.get_template('facultad_update.html')
    id = request.GET['id']
    print(id)

    filtrar = facultad.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = facultadForm(initial={'nombre': filtrar[0].nombre, 'universidad':filtrar[0].universidad})
    #form = universidadForm()

    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = facultad.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_facultad = request.POST.get('nombre', '')
            id_universidad = request.POST.get('universidad','')
            id_universidad = universidad.objects.all().filter(id=id_universidad)
            # insertamos y guardamos los datos del formulario en BD
            db_register = facultad()
            db_register.id = id
            db_register.nombre = nombre_facultad
            db_register.universidad = id_universidad[0]
            db_register.save()
            return HttpResponseRedirect('/facultadlist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def facultadDeleteView(request):
    template = loader.get_template('facultad_delete.html')
    id = request.GET['id']

    filtrar = facultad.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = facultadForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = facultad.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/facultadlist')
############################################################################################
def carreraView(request):
    template = loader.get_template('carrera.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = carreraForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_carrera = request.POST.get('nombre','')
        id_facultad = request.POST.get('facultad','')
        id_facultad = facultad.objects.all().filter(id=id_facultad)
        #insertamos y guardamos los datos del formulario en BD
        db_register = carrera()
        db_register.nombre = nombre_carrera
        db_register.facultad = id_facultad[0]
        db_register.save()
        return HttpResponseRedirect('/carreralist')
    return HttpResponse(template.render(contex, request))

def carreralistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = carrera.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "carrera_list.html" , contex )

def carreraUpdateView(request):
    template = loader.get_template('carrera_update.html')
    id = request.GET['id']
    print(id)

    filtrar = carrera.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = carreraForm(initial={'nombre': filtrar[0].nombre, 'facultad':filtrar[0].facultad})
    #form = universidadForm()

    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = carrera.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_carrera = request.POST.get('nombre', '')
            id_facultad = request.POST.get('facultad','')
            id_facultad = facultad.objects.all().filter(id=id_facultad)
            # insertamos y guardamos los datos del formulario en BD
            db_register = carrera()
            db_register.id = id
            db_register.nombre = nombre_carrera
            db_register.facultad = id_facultad[0]
            db_register.save()
            return HttpResponseRedirect('/carreralist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def carreraDeleteView(request):
    template = loader.get_template('carrera_delete.html')
    id = request.GET['id']

    filtrar = carrera.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = carreraForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = carrera.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/carreralist')

#####################################################################################################################
def tipo_usuarioView(request):
    template = loader.get_template('tipo_usuario.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = tipo_usuarioForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_tipo_usuario = request.POST.get('nombre','')
        #insertamos y guardamos los datos del formulario en BD
        db_register = tipo_usuario()
        db_register.nombre = nombre_tipo_usuario
        db_register.save()
        return HttpResponseRedirect('/tipo_usuariolist')
    return HttpResponse(template.render(contex, request))

def tipo_usuariolistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = tipo_usuario.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "tipo_usuario_list.html" , contex )

def tipo_usuarioUpdateView(request):
    template = loader.get_template('tipo_usuario_update.html')
    id = request.GET['id']
    print(id)
    filtrar = tipo_usuario.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = tipo_usuarioForm(initial={'nombre': filtrar[0].nombre})
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = tipo_usuario.objects.all()
    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_tipo_usuario = request.POST.get('nombre', '')
            # insertamos y guardamos los datos del formulario en BD
            db_register = tipo_usuario()
            db_register.id = id
            db_register.nombre = nombre_tipo_usuario
            db_register.save()
            return HttpResponseRedirect('/tipo_usuariolist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def tipo_usuarioDeleteView(request):
    template = loader.get_template('tipo_usuario_delete.html')
    id = request.GET['id']

    filtrar = tipo_usuario.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = tipo_usuarioForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = tipo_usuario.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/tipo_usuariolist')

#######################################################################################################################
def usuarioView(request):
    template = loader.get_template('usuario.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = usuarioForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_usuario = request.POST.get('nombre','')
        apellidos_usuario = request.POST.get('apellidos','')
        rut_usuario = request.POST.get('rut','')
        correo_usuario = request.POST.get('correo','')
        contrasena_usuario = request.POST.get('contrasena','')
        id_carrera = request.POST.get('carrera','')
        id_carrera = carrera.objects.all().filter(id=id_carrera)
        id_tipo_usuario = request.POST.get('tipo_usuario','')
        id_tipo_usuario = tipo_usuario.objects.all().filter(id=id_tipo_usuario)
        #insertamos y guardamos los datos del formulario en BD
        db_register = usuario()
        db_register.nombre = nombre_usuario
        db_register.apellidos = apellidos_usuario
        db_register.rut = rut_usuario
        db_register.correo = correo_usuario
        db_register.contrasena = contrasena_usuario
        db_register.carrera = id_carrera[0]
        db_register.tipo_usuario = id_tipo_usuario[0]
        db_register.save()
        return HttpResponseRedirect('/usuariolist')
    return HttpResponse(template.render(contex, request))

def usuariolistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = usuario.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "usuariolist.html" , contex )

def usuarioUpdateView(request):
    template = loader.get_template('usuario_update.html')
    id = request.GET['id']
    print(id)

    filtrar = usuario.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = usuarioForm(initial={'nombre': filtrar[0].nombre, 'apellidos':filtrar[0].apellidos, 'rut':filtrar[0].rut,
        'correo':filtrar[0].correo,'contrasena':filtrar[0].contrasena,'carrera':filtrar[0].carrera,'tipo_usuario':filtrar[0].tipo_usuario})
    #form = universidadForm()

    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = usuario.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_usuario = request.POST.get('nombre','')
            apellidos_usuario = request.POST.get('apellidos','')
            rut_usuario = request.POST.get('rut','')
            correo_usuario = request.POST.get('correo','')
            contrasena_usuario = request.POST.get('contrasena','')
            id_carrera = request.POST.get('carrera','')
            id_carrera = carrera.objects.all().filter(id=id_carrera)
            id_tipo_usuario = request.POST.get('tipo_usuario','')
            id_tipo_usuario = tipo_usuario.objects.all().filter(id=id_tipo_usuario)
            # insertamos y guardamos los datos del formulario en BD
            db_register = usuario()
            db_register.id = id
            db_register.nombre = nombre_usuario
            db_register.apellidos = apellidos_usuario
            db_register.rut = rut_usuario
            db_register.correo = correo_usuario
            db_register.contrasena = contrasena_usuario
            db_register.carrera = id_carrera[0]
            db_register.tipo_usuario = id_tipo_usuario[0]
            db_register.save()
            return HttpResponseRedirect('/usuariolist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def usuarioDeleteView(request):
    template = loader.get_template('usuario_delete.html')
    id = request.GET['id']

    filtrar = usuario.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = usuarioForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = usuario.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/usuariolist')

#######################################################################################################################
def moduloView(request):
    template = loader.get_template('modulo.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = moduloForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_modulo = request.POST.get('nombre','')
        id_carrera = request.POST.get('carrera','')
        id_carrera= carrera.objects.all().filter(id=id_carrera)
        #insertamos y guardamos los datos del formulario en BD
        db_register = modulo()
        db_register.nombre = nombre_modulo
        db_register.carrera = id_carrera[0]
        db_register.save()
        return HttpResponseRedirect('/modulolist')
    return HttpResponse(template.render(contex, request))

def modulolistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = modulo.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "modulo_list.html" , contex )

def moduloUpdateView(request):
    template = loader.get_template('modulo_update.html')
    id = request.GET['id']
    print(id)
    filtrar = modulo.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = moduloForm(initial={'nombre': filtrar[0].nombre})
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = modulo.objects.all()
    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_modulo = request.POST.get('nombre', '')
            id_carrera = request.POST.get('carrera','')
            id_carrera= carrera.objects.all().filter(id=id_carrera)
            # insertamos y guardamos los datos del formulario en BD
            db_register = modulo()
            db_register.id = id
            db_register.nombre = nombre_modulo
            db_register.carrera = id_carrera[0]
            db_register.save()
            return HttpResponseRedirect('/modulolist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def moduloDeleteView(request):
    template = loader.get_template('modulo_delete.html')
    id = request.GET['id']

    filtrar = modulo.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = moduloForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = modulo.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/modulolist')

#######################################################################################################################

def tipo_evaluacionView(request):
    template = loader.get_template('tipo_evaluacion.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = tipo_evaluacionForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nombre_tipo_evaluacion = request.POST.get('nombre','')
        #insertamos y guardamos los datos del formulario en BD
        db_register = tipo_evaluacion()
        db_register.nombre = nombre_tipo_evaluacion
        db_register.save()
        return HttpResponseRedirect('/tipo_evaluacionlist')
    return HttpResponse(template.render(contex, request))

def tipo_evaluacionlistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = tipo_evaluacion.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "tipo_evaluacion_list.html" , contex )

def tipo_evaluacionUpdateView(request):
    template = loader.get_template('tipo_evaluacion_update.html')
    id = request.GET['id']
    print(id)
    filtrar = tipo_evaluacion.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = tipo_evaluacionForm(initial={'nombre': filtrar[0].nombre})
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = tipo_evaluacion.objects.all()
    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():
            # recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nombre_tipo_evaluacion = request.POST.get('nombre', '')
            # insertamos y guardamos los datos del formulario en BD
            db_register = tipo_evaluacion()
            db_register.id = id
            db_register.nombre = nombre_tipo_evaluacion
            db_register.save()
            return HttpResponseRedirect('/tipo_evaluacionlist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def tipo_evaluacionDeleteView(request):
    template = loader.get_template('tipo_evaluacion_delete.html')
    id = request.GET['id']

    filtrar = tipo_evaluacion.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = tipo_evaluacionForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = tipo_evaluacion.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/tipo_evaluacionlist')

#######################################################################################################################


def evaluacionView(request):
    template = loader.get_template('evaluacion.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = evaluacionForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        nota_final_evaluacion = request.POST.get('nota_final','')
        id_tipo_evaluacion = request.POST.get('tipo_evaluacion','')
        id_tipo_evaluacion = tipo_evaluacion.objects.all().filter(id=id_tipo_evaluacion)
        id_modulo = request.POST.get('modulo','')
        id_modulo = modulo.objects.all().filter(id=id_modulo)
        ponderacion_evaluacion = request.POST.get('ponderacion','')
        fecha_evaluacion = request.POST.get('fecha','')

        #insertamos y guardamos los datos del formulario en BD
        db_register = evaluacion()
        db_register.nota_final = nota_final_evaluacion
        db_register.tipo_evaluacion = id_tipo_evaluacion[0]
        db_register.modulo = id_modulo[0]
        db_register.ponderacion = ponderacion_evaluacion
        db_register.fecha = fecha_evaluacion
        db_register.save()
        return HttpResponseRedirect('/evaluacionlist')
    return HttpResponse(template.render(contex, request))

def evaluacionlistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = evaluacion.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "evaluacion_list.html" , contex )

def evaluacionUpdateView(request):
    template = loader.get_template('evaluacion_update.html')
    id = request.GET['id']
    #print(id)
    filtrar = evaluacion.objects.all().filter(id=id)
    #print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = evaluacionForm(initial={'nota_final': filtrar[0].nota_final, 'modulo':filtrar[0].modulo,
                        'ponderacion':filtrar[0].ponderacion, 'fecha':filtrar[0].fecha, 'tipo_evaluacion':filtrar[0].tipo_evaluacion})
    #form = universidadForm()
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = evaluacion.objects.all()
    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():

            #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            nota_final_evaluacion = request.POST.get('nota_final','')
            id_tipo_evaluacion = request.POST.get('tipo_evaluacion','')
            id_tipo_evaluacion= tipo_evaluacion.objects.all().filter(id=id_tipo_evaluacion)
            id_modulo = request.POST.get('modulo','')
            id_modulo= modulo.objects.all().filter(id=id_modulo)
            ponderacion_evaluacion = request.POST.get('ponderacion','')
            fecha_evaluacion = request.POST.get('fecha','')

            #insertamos y guardamos los datos del formulario en BD
            db_register = evaluacion()
            db_register.nota_final = nota_final_evaluacion
            db_register.tipo_evaluacion = id_tipo_evaluacion[0]
            db_register.modulo = id_modulo[0]
            db_register.ponderacion = ponderacion_evaluacion
            db_register.fecha = fecha_evaluacion
            db_register.save()
            return HttpResponseRedirect('/evaluacionlist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def evaluacionDeleteView(request):
    template = loader.get_template('evaluacion_delete.html')
    id = request.GET['id']

    filtrar = evaluacion.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = evaluacionForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = evaluacion.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/evaluacionlist')

#######################################################################################################################

def itemView(request):
    template = loader.get_template('item.html')
    #definimos diccionario
    contex = {}
    #definimos el metodo POST para pasar los datos
    form = itemForm(request.POST)
    #añadimos datos al diccionario
    contex['formulario'] = form
    #validamos si el formaulario entrega datos
    if form.is_valid():
        print("TEST")
        #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
        id_evaluacion = request.POST.get('evaluacion','')
        id_evaluacion = evaluacion.objects.all().filter(id=id_evaluacion)
        print(id_evaluacion)
        descripcion_item = request.POST.get('descripcion','')
        puntaje_item = request.POST.get('puntaje','')
        ponderacion_item = request.POST.get('ponderacion','')

        #insertamos y guardamos los datos del formulario en BD
        db_register = item()
        db_register.evaluacion = id_evaluacion[0]
        db_register.descripcion =  descripcion_item
        db_register.puntaje = puntaje_item
        db_register.ponderacion = ponderacion_item
        db_register.save()
        return HttpResponseRedirect('/itemlist')
    return HttpResponse(template.render(contex, request))

def itemlistView(request):
    #definimos diccionario
    contex = {}
     #es un selec * from a tipoUsuario para mostrar datos en tabla

    query = item.objects.all()
    #añadimos datos al diccionario

    contex['datos'] = query

    return render(request, "item_list.html" , contex )

def itemUpdateView(request):
    template = loader.get_template('item_update.html')
    id = request.GET['id']
    print(id)
    filtrar = item.objects.all().filter(id=id)
    print(filtrar)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    # form = tipoUsuarioForm(instance = filtrar)
    form = itemForm(initial={'descripcion': filtrar[0].descripcion})
    #form = universidadForm()
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = item.objects.all()
    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    if request.GET:
        print("es GET")

    if request.POST:
        print("es POST")
        if not form.is_valid():

            #recibimos datos del formulario en variables , entre () especificamos el nombre de la variable del formulario
            descripcion_item = request.POST.get('descripcion','')
            puntaje_item= request.POST.get('puntaje','')
            ponderacion_item = request.POST.get('ponderacion','')
            id_evaluacion = request.POST.get('evaluacion','')
            id_evaluacion= evaluacion.objects.all().filter(id=id_evaluacion)
            #insertamos y guardamos los datos del formulario en BD
            db_register = item()
            db_register.descripcion =  descripcion_item
            db_register.puntaje = puntaje_item
            db_register.ponderacion = ponderacion_item
            db_register.evaluacion = id_evaluacion[0]
            db_register.save()
            return HttpResponseRedirect('/itemlist')
    return HttpResponse(template.render(contex, request))
    #return HttpResponseRedirect('/ciudad')

def itemDeleteView(request):
    template = loader.get_template('item_delete.html')
    id = request.GET['id']

    filtrar = item.objects.get(id=id)
    # definimos diccionario
    contex = {}
    # definimos el metodo POST para pasar los datos
    form = itemForm(request.POST)
    # es un selec * from a tipoUsuario para mostrar datos en tabla
    query = item.objects.all()

    # añadimos datos al diccionario
    contex['datos'] = query
    contex['formulario'] = form

    filtrar.delete()

    #return HttpResponse(template.render(contex, request))
    return HttpResponseRedirect('/itemlist')
