from django.test import TestCase
from .models import *
from django.http import Http404
from django.urls import reverse
from django.utils import timezone


######################################################################################
class ciudadModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Ciudad ***********")
        # Set up non-modified objects used by all test methods
        ciudad.objects.create(nombre='Talca')
        print("Test de ingresar datos a ciudad se hizo de forma correcta")

    def test_nombre_label(self):
        ciudad_test = ciudad.objects.get(id=2)
        field_label = ciudad_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de ciudad se ejecutó correctamente")

    def test_nombre_max_length(self):
        ciudad_max = ciudad.objects.get(id=2)
        max_length = ciudad_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de ciudad se ejecutó correctamente")

    def test_ciudad_name_is_nombre(self):
        ciudad_name = ciudad.objects.get(id=2)
        expected_object_name = f'{ciudad_name.nombre}'
        self.assertEquals(expected_object_name, str(ciudad_name))
        print("Test nombre de la ciudad se ejecutó correctamente")


######################################################################################
class universidadModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Universidad ***********")
        # Set up non-modified objects used by all test methods
        ciudad_universidad=ciudad.objects.create(nombre='ciudad')
        universidad.objects.create(nombre='UTALCA', ciudad=ciudad_universidad)
        print("Test de ingresar datos a universidad se hizo de forma correcta")

    def test_nombre_label(self):
        universidad_test = universidad.objects.get(id=6)
        field_label = universidad_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de universidad se hizo de forma correcta")

    def test_nombre_max_length(self):
        universidad_max = universidad.objects.get(id=6)
        max_length = universidad_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de universidad se hizo de forma correcta")

    def test_universidad_name_is_nombre(self):
        universidad_name = universidad.objects.get(id=6)
        expected_object_name = f'{universidad_name.nombre}'
        self.assertEquals(expected_object_name, str(universidad_name))
        print("Test nombre de la universidad se hizo de forma correcta")



######################################################################################
class facultadModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Facultad ***********")
        # Set up non-modified objects used by all test methods
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad.objects.create(id=5,nombre='Ingenieria', universidad=universidad_facultad)
        print("Test de ingresar datos a facultad se hizo de forma correcta")

    def test_nombre_label(self):
        facultad_test = facultad.objects.get(id=5)
        field_label = facultad_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de facultad se hizo de forma correcta")

    def test_nombre_max_length(self):
        facultad_max = facultad.objects.get(id=5)
        max_length = facultad_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de facultad se hizo de forma correcta")

    def test_facultad_name_is_nombre(self):
        facultad_name = facultad.objects.get(id=5)
        expected_object_name = f'{facultad_name.nombre}'
        self.assertEquals(expected_object_name, str(facultad_name))
        print("Test nombre de la facultad se hizo de forma correcta")


######################################################################################
class carreraModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Carrera ***********")
        # Set up non-modified objects used by all test methods
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera.objects.create(nombre='Medicina', facultad=facultad_carrera)
        print("Test de ingresar datos a carrera se hizo de forma correcta")

    def test_nombre_label(self):
        carrera_test = carrera.objects.get(id=1)
        field_label = carrera_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de carrera se hizo de forma correcta")

    def test_nombre_max_length(self):
        carrera_max = carrera.objects.get(id=1)
        max_length = carrera_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de carrera se hizo de forma correcta")

    def test_carrera_name_is_nombre(self):
        carrera_name = carrera.objects.get(id=1)
        expected_object_name = f'{carrera_name.nombre}'
        self.assertEquals(expected_object_name, str(carrera_name))
        print("Test nombre de la carrera se hizo de forma correcta")


######################################################################################
class tipoUsuaioModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Tipo de Usuario ***********")
        # Set up non-modified objects used by all test methods
        tipo_usuario.objects.create(nombre='Alumno')
        print("Test de ingresar datos a tipo_usuario se hizo de forma correcta")

    def test_nombre_label(self):
        tipo_usuario_test = tipo_usuario.objects.get(id=1)
        field_label = tipo_usuario_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de tipo_usuario se ejecutó correctamente")

    def test_nombre_max_length(self):
        tipo_usuario_max = tipo_usuario.objects.get(id=1)
        max_length = tipo_usuario_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de tipo_usuario se ejecutó correctamente")

    def test_tipo_usuario_name_is_nombre(self):
        tipo_usuario_name = tipo_usuario.objects.get(id=1)
        expected_object_name = f'{tipo_usuario_name.nombre}'
        self.assertEquals(expected_object_name, str(tipo_usuario_name))
        print("Test nombre de tipo_usuario se ejecutó correctamente")


######################################################################################
class usuarioModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Usuario ***********")
        # Set up non-modified objects used by all test methods
        tipo_usuario2=tipo_usuario.objects.create(nombre='Alumno')
        ciudad_usuario=ciudad.objects.create(nombre='Talca')
        universidad_usuario=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_usuario)
        facultad_usuario=facultad.objects.create(nombre='Ingenieria', universidad=universidad_usuario)
        carrera_usuario=carrera.objects.create(nombre='Medicina', facultad=facultad_usuario)
        usuario.objects.create(id=6,nombre='elraul',apellidos='rojas',rut='22878987',
        correo='elbartojoma',contrasena='1235',carrera=carrera_usuario,tipo_usuario = tipo_usuario2,)
        print("Test de ingresar datos a usuario se hizo de forma correcta")

    def test_nombre_label(self):
        usuario_test = usuario.objects.get(id=6)
        field_label = usuario_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de usuario se hizo de forma correcta")

    def test_nombre_max_length(self):
        usuario_max = usuario.objects.get(id=6)
        max_length = usuario_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de usuario se hizo de forma correcta")

    def test_usuario_name_is_nombre(self):
        usuario_name = usuario.objects.get(id=6)
        expected_object_name = f'{usuario_name.nombre}'
        self.assertEquals(expected_object_name, str(usuario_name))
        print("Test nombre de usuario se hizo de forma correcta")

    def test_apellidos_label(self):
        usuario_test = usuario.objects.get(id=6)
        field_label = usuario_test._meta.get_field('apellidos').verbose_name
        self.assertEquals(field_label, 'apellidos')
        print("Test label apellidos de usuario se hizo de forma correcta")

    def test_apellidos_max_length(self):
        usuario_max = usuario.objects.get(id=6)
        max_length = usuario_max._meta.get_field('apellidos').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de apellidos de usuario se hizo de forma correcta")

    def test_usuario_apellidos_is_apellido(self):
        usuario_apellidos = usuario.objects.get(id=6)
        expected_object_name = f'{usuario_apellidos.apellidos}'
        self.assertEquals(expected_object_name, str(usuario_apellidos.apellidos))
        print("Test apellidos de usuario se hizo de forma correcta")

    def test_rut_label(self):
        usuario_test = usuario.objects.get(id=6)
        field_label = usuario_test._meta.get_field('rut').verbose_name
        self.assertEquals(field_label, 'rut')
        print("Test label rut de usuario se hizo de forma correcta")

    def test_rut_max_length(self):
        usuario_max = usuario.objects.get(id=6)
        max_length = usuario_max._meta.get_field('rut').max_length
        self.assertEquals(max_length, 10)
        print("Test max length de rut de usuario se hizo de forma correcta")

    def test_usuario_rut_is_rut(self):
        usuario_rut = usuario.objects.get(id=6)
        expected_object_name = f'{usuario_rut.rut}'
        self.assertEquals(expected_object_name, str(usuario_rut.rut))
        print("Test rut de usuario se hizo de forma correcta")

    def test_correo_label(self):
        usuario_test = usuario.objects.get(id=6)
        field_label = usuario_test._meta.get_field('correo').verbose_name
        self.assertEquals(field_label, 'correo')
        print("Test label correo de usuario se hizo de forma correcta")

    def test_correo_max_length(self):
        usuario_max = usuario.objects.get(id=6)
        max_length = usuario_max._meta.get_field('correo').max_length
        self.assertEquals(max_length, 25)
        print("Test max length de correo de usuario se hizo de forma correcta")

    def test_usuario_correo_is_correo(self):
        usuario_correo = usuario.objects.get(id=6)
        expected_object_name = f'{usuario_correo.correo}'
        self.assertEquals(expected_object_name, str(usuario_correo.correo))
        print("Test correo de usuario se hizo de forma correcta")

    def test_contrasena_label(self):
        usuario_test = usuario.objects.get(id=6)
        field_label = usuario_test._meta.get_field('contrasena').verbose_name
        self.assertEquals(field_label, 'contrasena')
        print("Test label contrasena de usuario se hizo de forma correcta")

    def test_contrasena_max_length(self):
        usuario_max = usuario.objects.get(id=6)
        max_length = usuario_max._meta.get_field('contrasena').max_length
        self.assertEquals(max_length, 1000)
        print("Test max length de contrasena de usuario se hizo de forma correcta")

    def test_usuario_contrasena_is_contrasena(self):
        usuario_contrasena = usuario.objects.get(id=6)
        expected_object_name = f'{usuario_contrasena.contrasena}'
        self.assertEquals(expected_object_name, str(usuario_contrasena.contrasena))
        print("Test contrasena de usuario se hizo de forma correcta")

######################################################################################
class moduloModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Modulo ***********")
        # Set up non-modified objects used by all test methods
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera_modulo=carrera.objects.create(nombre='Medicina',facultad=facultad_carrera)
        modulo.objects.create(nombre='Software', carrera=carrera_modulo)
        print("Test de ingresar datos a modulo se hizo de forma correcta")

    def test_nombre_label(self):
        modulo_test = modulo.objects.get(id=3)
        field_label = modulo_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de modulo se ejecutó correctamente")

    def test_nombre_max_length(self):
        modulo_max = modulo.objects.get(id=3)
        max_length = modulo_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de modulo se ejecutó correctamente")

    def test_modulo_name_is_nombre(self):
        modulo_name = modulo.objects.get(id=3)
        expected_object_name = f'{modulo_name.nombre}'
        self.assertEquals(expected_object_name, str(modulo_name))
        print("Test nombre de la modulo se ejecutó correctamente")



######################################################################################

class tipo_evaluacionModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de tipo_evaluacion ***********")
        # Set up non-modified objects used by all test methods
        tipo_evaluacion.objects.create(nombre='prueba')
        print("Test de ingresar datos a tipo_evaluacion se hizo de forma correcta")

    def test_nombre_label(self):
        tipo_evaluacion_test = tipo_evaluacion.objects.get(id=3)
        field_label = tipo_evaluacion_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de tipo_evaluacion se ejecutó correctamente")

    def test_nombre_max_length(self):
        tipo_evaluacion_max = tipo_evaluacion.objects.get(id=3)
        max_length = tipo_evaluacion_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 50)
        print("Test max length de nombre de tipo_evaluacion se ejecutó correctamente")

    def test_tipo_evaluacion_name_is_nombre(self):
        tipo_evaluacion_name = tipo_evaluacion.objects.get(id=3)
        expected_object_name = f'{tipo_evaluacion_name.nombre}'
        self.assertEquals(expected_object_name, str(tipo_evaluacion_name))
        print("Test nombre de tipo_evaluacion se ejecutó correctamente")


######################################################################################


class evaluacionModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Evaluacion ***********")
        # Set up non-modified objects used by all test methods
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera_modulo=carrera.objects.create(nombre='Medicina',facultad=facultad_carrera)
        modulo_evaluacion=modulo.objects.create(nombre='Software',carrera=carrera_modulo)
        tipo_evaluacion_evaluacion=tipo_evaluacion.objects.create(nombre='prueba')
        evaluacion.objects.create(nota_final=6.6, tipo_evaluacion=tipo_evaluacion_evaluacion,
        modulo=modulo_evaluacion, ponderacion=40, fecha=timezone.now())
        print("Test de ingresar datos a evaluacion se hizo de forma correcta")

    def test_ponderacion_label(self):
        evaluacion_test = evaluacion.objects.get(id=1)
        field_label = evaluacion_test._meta.get_field('ponderacion').verbose_name
        self.assertEquals(field_label, 'ponderacion')
        print("Test label ponderacion de la evaluacion se ejecutó correctamente")



######################################################################################
class itemModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Item ***********")
        # Set up non-modified objects used by all test methods
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera_modulo=carrera.objects.create(nombre='Medicina',facultad=facultad_carrera)
        modulo_evaluacion=modulo.objects.create(nombre='Software',carrera=carrera_modulo)
        tipo_evaluacion_evaluacion=tipo_evaluacion.objects.create(nombre='prueba')
        evaluacion_item=evaluacion.objects.create(nota_final=7.0, tipo_evaluacion=tipo_evaluacion_evaluacion,
        modulo=modulo_evaluacion, ponderacion=40, fecha=timezone.now())
        item.objects.create(descripcion='esta_es_descripcion',puntaje=15,ponderacion=40,
        evaluacion=evaluacion_item)
        print("Test de ingresar datos a evaluacion se hizo de forma correcta")

    def test_descripcion_label(self):
        item_test = item.objects.get(id=1)
        field_label = item_test._meta.get_field('descripcion').verbose_name
        self.assertEquals(field_label, 'descripcion')
        print("Test label descripcion del item se ejecutó correctamente")

    def test_item_descripcion_is_descripcion(self):
        item_descripcion = item.objects.get(id=1)
        expected_object_name = f'{item_descripcion.descripcion}'
        self.assertEquals(expected_object_name, str(item_descripcion))
        print("Test descripcion de item se ejecutó correctamente")

    def test_puntaje_label(self):
        item_test = item.objects.get(id=1)
        field_label = item_test._meta.get_field('puntaje').verbose_name
        self.assertEquals(field_label, 'puntaje')
        print("Test label puntaje de la evaluacion se ejecutó correctamente")

    def test_ponderacion_label(self):
        item_test = item.objects.get(id=1)
        field_label = item_test._meta.get_field('ponderacion').verbose_name
        self.assertEquals(field_label, 'ponderacion')
        print("Test label ponderacion de la evaluacion se ejecutó correctamente")
