from django.apps import AppConfig


class PesAppConfig(AppConfig):
    name = 'pes_app'
