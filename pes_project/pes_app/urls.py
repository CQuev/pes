#from django.conf.urls import url
#from django.conf import settings
#from django.conf.urls.static import static
#from pes_app import views
#from django.urls import path
#from django.contrib.auth.views import *
#from . import views

##############################################
from django.conf.urls import url
from .views import *
from django.conf.urls.static import static
from django.conf  import settings
from django.contrib.auth import login
urlpatterns = [
    url(r'^index/$', indexView, name='index'),
    ###############################################################
    url(r'^ciudad/$', ciudadView, name='ciudad'),
    url(r'^ciudadlist/$', ciudadlistView, name='ciudadlist'),
    url(r'^ciudadupdate/$', ciudadUpdateView, name='ciudadupdate'),
    url(r'^ciudaddelete/$', ciudadDeleteView, name='ciudaddelete'),
    ###############################################################
    url(r'^universidad/$', universidadView, name='universidad'),
    url(r'^universidadlist/$', universidadlistView, name='universidadlist'),
    url(r'^universidadupdate/$', universidadUpdateView, name='universidadupdate'),
    url(r'^universidaddelete/$', universidadDeleteView, name='universidaddelete'),
    ###############################################################
    url(r'^facultad/$', facultadView, name='facultad'),
    url(r'^facultadlist/$', facultadlistView, name='facultadlist'),
    url(r'^facultadupdate/$', facultadUpdateView, name='facultadupdate'),
    url(r'^facultaddelete/$', facultadDeleteView, name='facultaddelete'),
    ###############################################################
    url(r'^carrera/$', carreraView, name='carrera'),
    url(r'^carreralist/$', carreralistView, name='carreralist'),
    url(r'^carreraupdate/$', carreraUpdateView, name='carreraupdate'),
    url(r'^carreradelete/$', carreraDeleteView, name='carreradelete'),
    ###############################################################
    url(r'^tipo_usuario/$', tipo_usuarioView, name='tipo_usuario'),
    url(r'^tipo_usuariolist/$', tipo_usuariolistView, name='tipo_usuariolist'),
    url(r'^tipo_usuarioupdate/$', tipo_usuarioUpdateView, name='tipo_usuarioupdate'),
    url(r'^tipo_usuariodelete/$', tipo_usuarioDeleteView, name='tipo_usuariodelete'),
    ###############################################################
    url(r'^usuario/$', usuarioView, name='usuario'),
    url(r'^usuariolist/$', usuariolistView, name='usuariolist'),
    url(r'^usuarioupdate/$', usuarioUpdateView, name='usuarioupdate'),
    url(r'^usuariodelete/$', usuarioDeleteView, name='usuariodelete'),
    ###############################################################
    url(r'^modulo/$', moduloView, name='modulo'),
    url(r'^modulolist/$', modulolistView, name='modulolist'),
    url(r'^moduloupdate/$', moduloUpdateView, name='moduloupdate'),
    url(r'^modulodelete/$', moduloDeleteView, name='modulodelete'),
    ###############################################################
    url(r'^tipo_evaluacion/$', tipo_evaluacionView, name='tipo_evaluacion'),
    url(r'^tipo_evaluacionlist/$', tipo_evaluacionlistView, name='tipo_evaluacionlist'),
    url(r'^tipo_evaluacionupdate/$', tipo_evaluacionUpdateView, name='tipo_evaluacionupdate'),
    url(r'^tipo_evaluaciondelete/$', tipo_evaluacionDeleteView, name='tipo_evaluaciondelete'),
    ###############################################################
    url(r'^evaluacion/$', evaluacionView, name='evaluacion'),
    url(r'^evaluacionlist/$', evaluacionlistView, name='evaluacionlist'),
    url(r'^evaluacionupdate/$', evaluacionUpdateView, name='evaluacionupdate'),
    url(r'^evaluaciondelete/$', evaluacionDeleteView, name='evaluaciondelete'),
    ###############################################################
    url(r'^item/$', itemView, name='item'),
    url(r'^itemlist/$', itemlistView, name='itemlist'),
    url(r'^itemupdate/$', itemUpdateView, name='itemupdate'),
    url(r'^itemdelete/$', itemDeleteView, name='itemdelete'),

]
