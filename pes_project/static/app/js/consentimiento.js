$( document ).ready(function() {
    console.log( "ready!" );
    $('#id_fecha').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
        startDate:	'2018/11/01'
    });
});